import React, { useContext } from 'react';
import { Navigate } from 'react-router-dom';
import ShopContext from '../ShopContext';

export default function ProtectedRoute({ children }) {
  const { state } = useContext(ShopContext);
  const { userInfo } = state;
  return userInfo ? children : <Navigate to="/login" />;
}
