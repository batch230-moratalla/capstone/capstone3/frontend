import React, { useContext } from 'react';
import { Navigate } from 'react-router-dom';
import ShopContext from '../ShopContext';

export default function AdminRoute({ children }) {
  const { state } = useContext(ShopContext);
  const { userInfo } = state;
  return userInfo && userInfo.isAdmin ? children : <Navigate to="/login" />;
}
